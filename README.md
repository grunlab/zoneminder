[![pipeline status](https://gitlab.com/grunlab/zoneminder/badges/main/pipeline.svg)](https://gitlab.com/grunlab/zoneminder/-/commits/main)

# GrunLab ZoneMinder

ZoneMinder non-root container image and deployment on Kubernetes.

Docs:
- https://docs.grunlab.net/images/zoneminder.md
- https://docs.grunlab.net/apps/zoneminder.md

Base image: [grunlab/base-image/ubuntu:22.04][base-image]

Format: docker

Supported architecture(s):
- arm-v7

[base-image]: <https://gitlab.com/grunlab/base-image>

