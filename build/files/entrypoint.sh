#!/bin/bash

sed -i "s/ZM_DB_HOST=localhost/ZM_DB_HOST=${ZM_DB_HOST}/g" /etc/zm/zm.conf
sed -i "s/ZM_DB_NAME=zm/ZM_DB_NAME=${ZM_DB_NAME}/g"        /etc/zm/zm.conf
sed -i "s/ZM_DB_USER=zmuser/ZM_DB_USER=${ZM_DB_USER}/g"    /etc/zm/zm.conf
sed -i "s/ZM_DB_PASS=zmpass/ZM_DB_PASS=${ZM_DB_PASS}/g"    /etc/zm/zm.conf

mysql -h${ZM_DB_HOST} -u${ZM_DB_USER} -p${ZM_DB_PASS} -D${ZM_DB_NAME} -e 'SELECT 1' > /dev/null 2>&1
if [ "$?" = "0" ];
then
  mysqlshow -h${ZM_DB_HOST} -u${ZM_DB_USER} -p${ZM_DB_PASS} ${ZM_DB_NAME} Config > /dev/null 2>&1
  if [ "$?" != "0" ];
  then
    mysql -h${ZM_DB_HOST} -u${ZM_DB_USER} -p${ZM_DB_PASS} < /usr/share/zoneminder/db/zm_create.sql > /dev/null 2>&1
  fi
  /usr/bin/zmpkg.pl start > /dev/null 2>&1
  apache2ctl -D FOREGROUND
else
  exit 1
fi

